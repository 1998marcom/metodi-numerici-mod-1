\documentclass[a4paper, 11pt]{article}
%\DeclareMathSizes{10}{11}{9}{8}
\usepackage[top=4cm, bottom=3cm, inner=2.8cm, outer=2.8cm]{geometry}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{wrapfig}
\hypersetup{colorlinks, linkcolor=blue}
\usepackage{float}
\usepackage{subcaption}
\newcommand{\dd}{\mathrm{d}}
\renewcommand{\tablename}{Tab.}
\renewcommand{\figurename}{Fig.}

\title{
	Metodi Numerici per la Fisica\\
	Modello di Ising in cristalli tridimensionali\footnote{
	\vspace{3pt} Sorgenti e pdf su 
	\url{https://gitlab.com/1998marcom/metodi-numerici-mod-1}}
	\\ \vspace{10pt}
    \small{PROGETTO MODULO 1}\\
}
\author{Marco Malandrone e Alessandro Falco}
%\author{Marco Malandrone\\\textbf{Scuola Normale Superiore\\ \vspace{10pt} Relatore: Prof. Massimo D'Elia}}

\date{\today}

\begin{document}

\maketitle

\tableofcontents

\section{Introduzione}
\subsection{Il modello}
Vogliamo studiare il comportamento di un cristallo di spin in prossimità della
temperatura critica. 
\subsubsection*{Modello di Ising}
Definiamo \textit{cristallo di spin} un grafo non orientato,
i cui vertici sono degli spin che possono assumere valori $s_i\in\{-1,+1\}$.
Due spin sono definiti \textit{primi vicini} se sono collegati da un
arco; l'Hamiltoniana è data da:
\begin{equation}
	H = -\frac12\sum_{\langle i, j \rangle}s_is_j - \sum_i B_is_i
\end{equation}
dove con $\sum_{\langle i, j \rangle}$ si intende una somma su tutte le coppie
ordinate di spin primi vicini e $B_i$ è un campo esterno fissato. Nel corso
delle simulazioni ci limiteremo al caso $B_i = 0 \; \forall i$.
\subsubsection*{Cristalli tridimensionali}
I grafi considerati in questo progetto sono delle strutture cristalline
tridimensionali. In particolare abbiamo scelto tutte strutture della famiglia
dei cristalli cubici:
\begin{itemize}
	\item cristalli cubici primitivi (il reticolo convenzionale),
	\item cristalli cubici a corpo centrato,
	\item cristalli cubici a facce centrate,
	\item cristalli cubici del tipo ``diamante''.
\end{itemize}

\subsection{Misure ed analisi}
\subsubsection*{Osservabili misurati}
Riportiamo la definizione della funzione di partizione:
\begin{equation}
	Z = \sum_\sigma \exp[-\beta H(\sigma)]
\end{equation}
dove $\sigma$ è una variabile a valori nelle configurazioni del sistema.

Abbiamo misurato durante le simulazioni, ogni 10 step di evoluzione, l'energia
interna media per spin:
\begin{equation}
	u = \frac UN = - \frac1N \partial_\beta\log Z = \frac1N \langle H \rangle
\end{equation}
e la magnetizzazione media per spin:
\begin{equation}
	m = \frac MN = \frac1{\beta N} \partial_{B} \log Z = 
	\frac1N \langle s \rangle
\end{equation}
dove abbiamo richiesto $B_i = B \; \forall i$.
\subsubsection*{Stime di $\beta_c, \nu, \beta$}
Definiamo $\xi$ la lunghezza di correlazione del sistema.
Il modello di Ising tridimensionale è caratterizzato da una transizione di fase
ad una temperatura critica $T_c$, in un intorno della quale
$\xi\rightarrow\infty$. Inoltre, definendo $t=\frac{T-T_c}{T_c}= 
-b = -\frac{\beta-\beta_c}{\beta_c}$:
\begin{align}
	\xi &\propto |t|^{-\nu} = |b|^{-\nu} \label{eq:nudef}\\
	|m| &\propto |t|^{\beta} \hspace{6.3pt} = |b|^{\beta}\label{eq:betadef}
\end{align}
Abbiamo poi usato i dati raccolti per ottenere delle stime dei due esponenti
critici, $\nu$ e $\beta$, e per stimare l'inverso della temperatura critica,
$\beta_c$, usando il metodo del \textit{finite size scaling}.
\subsubsection*{FSS}
Combinando le equazioni \ref{eq:nudef} e \ref{eq:betadef}: 
\begin{equation}
	|m| \propto \xi^{-\beta/\nu}
	\label{eq:mpx}
\end{equation}
Chiaramente però le dimensioni del sistema sono finite. Definiamo $L$ come la
lunghezza di uno spigolo del cristallo: sarebbe allora più opportuno riscrivere:
\begin{equation}
	|m| = L^{-\beta/\nu} \;\mu\left(L/\xi\right)
	\label{eq:mpxc}
\end{equation}
dove $\mu(L/\xi)$ è una opportuna funzione tale che:
\begin{equation}
	\mu(L/\xi) = 
	\begin{cases}
		c_1 (\xi/L)^{-\beta/\nu} &\text{se } \xi \ll L\\
		c_2 &\text{se } \xi \gg L 
	\end{cases}
\end{equation}
Lavorando in un intorno della temperatura critica possiamo riscrivere la
relazione \ref{eq:mpxc}:
\begin{equation}
	\tilde{\mu}(L^{1/\nu}|b|) = |m|\, L^{\beta/\nu}
	\label{eq:finalfss}
\end{equation}
Nell'equazione \ref{eq:finalfss}, la funzione $\tilde \mu$ non ha altra
dipendenza da $L$ se non quella che viene dall'argomento della stessa.
In altre parole, possiamo stimare i valori corretti di $\nu, \beta, \beta_c$ 
richiedendo che le funzioni $\tilde\mu_L$, i cui grafici possono essere ottenuti
a partire dalle simulazioni, siano in realtà un'unica funzione.

Nel caso di questo progetto abbiamo usato come metrica della coincidenza delle
$\tilde \mu_L$ l'integrale della varianza fra le curve.

\subsubsection*{\textit{Multiple histograms method}}
Per stimare l'integrale della varianza fra le curve è però necessario poter
ricavare il valore delle curve per tutte le temperature in un certo range.
Però le simulazioni sono eseguite ad intervalli di temperature ben definiti, e
in generale la coordinata $x$ associata alla funzione $\tilde\mu$, 
$L^{1/\nu}|b|$, è diversa per le varie simulazioni e dipende anche dai parametri
del fit che vogliamo determinare! Abbiamo quindi la
necessità di ricavare, a partire dai dati delle simulazioni, il valore delle
curve a temperature diverse da quelle delle simulazioni stesse.

Per realizzare ciò, abbiamo adottato come mezzo di ripesamento dei dati il
metodo degli ``istogrammi multipli''.

Supponiamo di avere simulato il nostro sistema a diverse temperature inverse
$\beta_k$, e aver misurato $n_k$ volte i dati in ogni simulazione. Ci chiediamo
ora: \textit{quante volte ci aspettiamo di avere misurato una energia $E$ nel
nostro sistema?}. Usando la funzione di partizione possiamo scrivere:
\begin{equation}
	N(E) = \sum_k \frac{n_k}{Z(\beta_k)}\rho(E) e^{-\beta_k E}
\end{equation}
Possiamo rigirare questa espressione per ottenere $\rho(E)$:
\begin{equation}
	\rho(E) = \frac{N(E)}{\sum_k n_k Z_k^{-1}e^{-\beta_kE}}
\end{equation}

Per definizione:
\begin{equation}
	Z(\beta) = \sum_E \rho(E) e^{-\beta E}
		= \sum_E \frac{N(E)}{\sum_k n_k Z_k^{-1}e^{-\beta_kE}} e^{-\beta E}
		\label{eq:ziter}
\end{equation}
Chiaramente il valore teorico di $N(E)$ non è noto a priori, ma è un dato che è
possibile stimare partendo dai record delle simulazioni. Partendo dalle
simulazioni può essere più conveniente riscrivere la \ref{eq:ziter} in termini
delle configurazioni $p$ campionate:
\begin{equation}
	Z(\beta) = 
	\sum_p \frac{1}{\sum_k n_k Z_k^{-1}e^{-\beta_kE}} e^{-\beta H(p)}
	\label{eq:ziterc}
\end{equation}
che può essere risolta per trovare le $Z_k$ iterativamente, nonché il valore di
$Z$ a qualunque temperatura.

Possiamo ora ricavare le nostre osservabili $|m|$ e $u$:
\begin{equation}
	|m(\beta)| = \frac1{Z(\beta)} 
	\sum_p \frac{|m(p)|}{\sum_k n_k Z_k^{-1}e^{-\beta_kE}} e^{-\beta H(p)}
	\label{eq:ziterc}
\end{equation}
\begin{equation}
	u(\beta) = \frac1{Z(\beta)} 
	\sum_p \frac{u(p)}{\sum_k n_k Z_k^{-1}e^{-\beta_kE}} e^{-\beta H(p)}
	\label{eq:ziterc}
\end{equation}

\section{Implementazione}
Abbiamo simulato il sistema usando una variante parallelizzata del
Metropolis-Hastings, intervallata ogni 100 tempi da una iterazione
dell'algoritmo di Wolff.
Abbiamo registrato i $m$ e $u$ ogni 10 step di evoluzione,
e usato come generatore di numeri pseudocasuali MT19937,
un generatore del tipo Mersenne-Twister.
La simulazione è implementata in \texttt{C++};
il codice sorgente è reperibile nel repository
\href{https://gitlab.com/1998marcom/metodi-numerici-mod-1}{metodi-numerici-mod-1}.

\subsection{Metropolis-Hastings \textit{parallelo}}
\subsubsection*{L'idea}
Il normale algortimo Metropolis-Hastings consente di far evolvere un sistema
cambiando il valore di uno spin alla volta.
Nel caso però in cui l'Hamiltoniana sia esprimibile 
come interazioni solamente fra siti primi vicini,
è possibile cambiare il valore dello spin in più di un sito alla volta,
prestando attenzione solo a variazioni locali dell'energia.
Tutto, però, a condizione di non cambiare 
in un'unica \textit{operazione} (nel seguito anche detta \textit{tick})
due spin primi vicini. 
\subsubsection*{L'algoritmo}
Ciascun \textit{tick} di evoluzione dell'algoritmo può essere diviso in due step.
Fra questi due step è opportuno assicurarsi che la memoria venga sincronizzata 
e tutti i core condividano la stessa memoria in lettura.
\begin{enumerate}
    \item Per ogni sito $n$ del sistema:
        \begin{itemize}
            \item Si sceglie con probabilità $p$ se cambiare lo spin.
			\item Si memorizzano $s'_n(t)$, il possibile valore al tempo
				successivo, e $s_n(t)$, il valore attuale dello spin,
				per lo step 2.
        \end{itemize}
    \item Per ogni sito $n$ del sistema:
        \begin{itemize}
            \item Se nello step 1 si è scelto di non cambiare lo spin, 
				assegna $s_n(t+1) = s_n(t)$.
            \item Se nello step 1 un qualunque spin primo vicino
                $m$ ha memorizzato un valore
                $s_m'(t)\neq s_m(t)$, assegna $s_n(t+1) = s_n(t)$.
            \item Se nessuna delle due precedenti condizioni è verificata, 
				calcola la variazione dell'hamiltoniana
                $\Delta H$ e assegna $s_n(t+1)$ con un algoritmo accept-reject:
                \[
                    s_n(t+1) =
                        \begin{cases}
                            s_n(t) &\text{if }\Delta H>0 
								\text{ with probability}\exp[-\beta (\Delta H)]\\
                            s'_n(t) &\mathrm{otherwise}
                        \end{cases}
                \]
        \end{itemize}
\end{enumerate}

\subsubsection*{Verifica del bilancio dettagliato}
La verifica della condizione del bilancio dettagliato discende dal
rispetto della condizione per il normale
Metropolis-Hastings, dato che è possibile considerare un \textit{tick} 
del Metropolis-Hastings parallelo come
una concatenazione di passi del normale Metropolis-Hastings in un qualunque 
ordine, non essendo questi passi in alcun modo dipendenti l'uno dall'altro.

%\subsection{Definire le celle fondamentali dei cristalli}
%Come le abbiamo definite. Qualche esempio...

\section{Analisi}
\subsection{Investigazioni preliminari}
Abbiamo innanzitutto fissato un valore di $L=10$ e abbiamo
cercato di capire quale fosse per i quattro cristalli la
temperatura critica, approssimativamente, per sapere in quale range di
temperature eseguire le simulazioni per diversi valori di $L$.
\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{images/single_magnetization_lattice.png}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{images/single_magnetization_body.png}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{images/single_magnetization_face.png}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{images/single_magnetization_diamond.png}
	\end{subfigure}
	\caption{
		Simulazioni dei 4 cristalli per	$L=10$,
		in prossimità della transizione di fase.
	}
	\label{fig:single}
\end{figure}

Dai dati mostrati in figura \ref{fig:single} abbiamo ricavato una prima stima
della temperatura critica:
\begin{itemize}
	\item standard lattice: $\beta_c \sim 0.22$,
	\item body centered cubic: $\beta_c \sim 0.11$,
	\item face centered cubic: $\beta_c \sim 0.13$,
	\item diamond cubic: $\beta_c \sim 0.23$.
\end{itemize}

\subsection{FSS}
Abbiamo poi eseguito le simulazioni per diversi valori di $L$ in prossimità
della temperatura critica stimata. Da queste simulazioni abbiamo estratto
un'unica curva per ogni valore di $L$, utilizzando il metodo degli istogrammi
multipli. I dati raccolti e le curve ricavate sono riportate in figura
\ref{fig:line}.
\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{images/line_magnetization_lattice.png}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{images/line_magnetization_body.png}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{images/line_magnetization_face.png}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{images/line_magnetization_diamond.png}
	\end{subfigure}
	\caption{
		Le croci indicano i dati ottenuti dalle simulazioni dei 4 cristalli 
		per diversi $L$. Le curve sono ottenute dai dati delle simulazioni 
		con il metodo degli istogrammi multipli.
	}
	\label{fig:line}
\end{figure}

Infine abbiamo minimizzato l'integrale della varianza fra le curve $\mu_L$,
facendo variare i parametri $\nu, \beta, \beta_c$, per ottenere una stima di
questi.

A titolo di confronto, in $D=3$ dimensioni la letteratura concorda 
sul fatto che che $\nu \approx 0.6300$, $\beta \approx 0.3264$.
\pagebreak

\subsubsection*{Standard lattice}
\begin{itemize}
	\item $\nu = 0.61 \pm 0.04 $
	\item $\beta = 0.31 \pm 0.07 $
	\item $\beta_c = 0.225 \pm 0.002 $
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{images/fit_magnetization_lattice.png}
\end{figure}
\subsubsection*{Body centered cubic}
\begin{itemize}
	\item $\nu = 0.63 \pm 0.07 $
	\item $\beta = 0.33 \pm 0.09 $
	\item $\beta_c = 0.1078 \pm 0.0007$
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{images/fit_magnetization_body.png}
\end{figure}
\subsubsection*{Face centered cubic}
\begin{itemize}
	\item $\nu = 0.59 \pm 0.05 $
	\item $\beta = 0.33 \pm 0.05 $
	\item $\beta_c = 0.1252 \pm 0.0004 $
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{images/fit_magnetization_face.png}
\end{figure}
\subsubsection*{Diamond cubic}
\begin{itemize}
	\item $\nu = 0.66 \pm 0.11 $
	\item $\beta = 0.41 \pm 0.17 $
	\item $\beta_c = 0.207 \pm 0.003 $
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{images/fit_magnetization_diamond.png}
\end{figure}




\end{document}
