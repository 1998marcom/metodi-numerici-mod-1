from pathlib import Path
import numpy as np
import subprocess as s
from tqdm import tqdm

main_exe = '' # To be initialized on module load

class Simulation:
    
    path = ""
    runned = False
    kwargs = {}
    data = {
        'u': np.array([]),
        'm': np.array([]),
    }
    
    def __init__(self, path, load=True):
        self.path = Path(path)
        if self.path.exists():
            self._load(headeronly=(not load))
            self.runned = True
            
    def run(self, load=True, **kwargs):
        params = {('-'+key): val for key, val in kwargs.items()}
        params['-o'] = str(self.path.resolve())
        run_args = [str(elem) for keyval in params.items() for elem in keyval]
        run_args = [str(Path(main_exe).resolve()), *run_args]
        #print(run_args)
        s.run(run_args)
        self.runned = True
        self._load(headeronly=(not load))
    
    def _load(self, headeronly=False):
        hf_path = Path(str(self.path) + '_header').resolve()
        with open(hf_path, 'r') as hf:
            keys = hf.readline().split()
            values = hf.readline().split()
            assert len(keys) == len(values)
            top = len(keys)
            self.kwargs = {}
            for i in range(top):
                self.kwargs[keys[i]] = values[i]
        if not headeronly:
            raw_data = np.loadtxt(self.path, unpack=False)
            self.data = {
                'u': raw_data[:, 0],
                'm': raw_data[:, 1],
            }

class Span:
    
    path = ""
    runned = False
    simulations = []
    
    def __init__(self, path, load=True):
        self.path = Path(path)
        if self.path.exists():
            self._load(load=load)
            self.runned = True
            
    def run(self, key, values, load=True, log=True, **kwargs):
        self.path.mkdir(exist_ok=False)
        for child in self.path.iterdir():
            child.unlink()
        params = kwargs
        if log:
            print("Span.run tqdm", flush=True)
            for value in tqdm(values):
                params[key] = value
                sim = Simulation( self.path / (str(key) + '_' + str(value)) )
                sim.run(**params)
        else:
            for value in values:
                params[key] = value
                sim = Simulation( self.path / (str(key) + '_' + str(value)) )
                sim.run(**params)
        self.runned = True
        self._load(load=load)
    
    def _load(self, load=True):
        #print("Span._load tqdm", flush=True)
        self.simulations = [
            Simulation(child, load=load) 
            for child in self.path.iterdir()
            if not (str(child).endswith('_header') or str(child).endswith('.ipynb_checkpoints'))
        ] # Assicuriamoci l'idempotenza
    def get_simulations(self):
        #print("Span.get_simulations tqdm", flush=True)
        for child in self.path.iterdir():
            if not (str(child).endswith('_header') or str(child).endswith('.ipynb_checkpoints')):
                yield Simulation(child)
        

from numpy.random import default_rng
rng = default_rng()


def bootstrap(xx, resample=100, blocksize=None):
    if blocksize is None:
        means = []
        for i in range(resample):
            indexes = rng.integers(low=0, high=len(xx), size=len(xx))
            sample = xx[indexes]
            means.append(np.mean(sample))
        return np.std(means)
    else:
        splits = len(xx)//blocksize
        yy = 1*xx[:splits*blocksize]
        yy = yy.reshape(splits, blocksize)
        ymeans = np.mean(yy, axis=1)
        means = []
        for i in range(resample):
            indexes = rng.integers(low=0, high=len(ymeans), size=len(ymeans))
            sample = ymeans[indexes]
            means.append(np.mean(sample))
        return np.std(means)