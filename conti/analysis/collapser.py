import simulation as sim
from pathlib import Path
import numpy as np
from tqdm import tqdm
from copy import copy
import matplotlib.pyplot as plt

sim.main_exe = '../build/crystal'


class LL: # Che nome del casso
    
    path = ""
    runned = False
    data = {
        4: {
            'b': np.array([]),
            'm': np.array([]),
            'm_err': np.array([]),
            'u': np.array([]),
            'u_err': np.array([]),
        }
    }
    
    def __init__(self, path, load=False, DISCARD=200, blocksize=None, resample=100):
        self.path = Path(path)
        if self.path.exists():
            self._load(DISCARD=DISCARD, load=load, blocksize=blocksize, resample=resample)
            self.runned = True
    
    def run(self, ll, bb, load=False, DISCARD=200, blocksize=None, resample=100, **kwargs):
        self.path.mkdir()
        for L in ll:
            print("Approaching L={}".format(L))
            params = copy(kwargs)
            params['L'] = L
            L_path = self.path / str(L)
            s = sim.Span(L_path)
            s.run('b', bb, **params, load=False, log=True)
        self.runned = True
        self._load(DISCARD=DISCARD, load=load, blocksize=blocksize, resample=resample)
    
    def _load(self, DISCARD=200, load=False, blocksize=None, resample=100):
        print("Loading spans", flush=True)
        self.spans = [
            sim.Span(child, load=load)
            for child in self.path.iterdir()
            if not str(child).endswith('.ipynb_checkpoints')
        ]
        self.data = {}
        for span in tqdm(self.spans):
            bb = []; mm = []; mm_err = []; uu = []; uu_err = []
            for simul in span.get_simulations():
                bb.append(float(simul.kwargs['b']))
                abs_mm = np.abs(simul.data['m'][DISCARD:])
                mm.append(np.mean(abs_mm))
                mm_err.append(sim.bootstrap(abs_mm, blocksize=blocksize, resample=resample))
                uu.append(np.mean(simul.data['u'][DISCARD:]))
                uu_err.append(sim.bootstrap(simul.data['u'][DISCARD:], blocksize=blocksize, resample=resample))
            self.data[int(span.simulations[0].kwargs['L'])] = {
                'b': np.array(bb),
                'm': np.array(mm),
                'm_err': np.array(mm_err),
                'u': np.array(uu),
                'u_err': np.array(uu_err),
            }
    
    def plot(self, b='b', m='m', m_err='m_err', bb=None, mm=None):
        for L, data in sorted(self.data.items(), key=lambda x: x[0]):
            data_line, caplines, barlinecols = plt.errorbar(
                data[b],
                data[m],
                data[m_err],
                fmt="x",
                label=str(L)
            )
            if bb:
                plt.plot(data[bb], data[mm], color=data_line.get_color())

    def check_loaded(self):
        return len(self.spans[0].simulations[0].data['u']) > 0

    def compute_Z(self, DISCARD=200, prec=0.98, log=True):
        if not self.check_loaded():
            raise Exception(
                "Vuoi veramente farmi girare da disco?" +
                "Credo proprio di no!"
            )
        for L, data in sorted(self.data.items(), key=lambda x: x[0]):
            if log:
                print("Examining L={}".format(L), flush=True)
            ## Compute the Z for the simulations we have
            span = [
                s for s in self.spans 
                if int(s.simulations[0].kwargs['L']) == L
            ][0]
            Z = np.array([1.0]*len(span.simulations), dtype=np.float128)
            betas = sorted(
                [float(simul.kwargs['b']) for simul in span.simulations]
            )
            ending = False
            if log:
                iterable = tqdm(range(10000))
            else:
                iterable = range(10000)
            for i in iterable:
                Zlast = Z
                Z = [
                    obs_avg(beta, span, Zlast, 
                                 DISCARD=DISCARD, loaded=True) 
                    for beta in betas
                ]
                if ending:
                    break
                if Z[0]/Zlast[0] > prec: # Non scassiamo con troppa precisione
                    Z = np.array(Z, dtype=np.float128)**2 / np.array(Zlast, dtype=np.float128)
                    ending = True
            if log:
                print("Zlast", Zlast)
                print("Z", Z)
            data['Z'] = Z # Oh sì non mi confonderò mai

    def compute_obs(
        self,
        name, # Name of the variable to store
        bb_name, # Name of the betas to use
        obs=lambda simul: 1.0,
            # Observable to measure: obs(simul) should return the observable
        ZZ_name='',
        DISCARD=200,
        ):
        if not 'Z' in list(self.data.values())[0].keys():
            self.compute_Z()
        for L, data in sorted(self.data.items(), key=lambda x: x[0]):
            bb = data[bb_name]
            span = [
                s for s in self.spans 
                if int(s.simulations[0].kwargs['L']) == L
            ][0]
            obs_result = np.array([
                obs_avg(b, span, data['Z'], obs=obs, 
                             DISCARD=DISCARD, loaded=True)
                for b in bb
            ], dtype=np.float128)
            if ZZ_name:
                obs_result /= np.array(data[ZZ_name], dtype=np.float128)
            data[name] = obs_result
    
    def curve_stddev_m(self, bb, nu, beta, beta_c, plot=False):
        # Note that bb is beta for L_min 
        # and beta is the critical exponent, not the inverse temperature
        """
        This function returns the stddev of the different curves,
        IN THE CASE OF THE MAGNETIZATION. MIGHT BE GENERALIZED BY CLASS CONFIG.
        You should optimize it to be 0 everywhere.
        If you opt for the least squares method, you are effectively
        minimizing the integral of the variance.
        We feel free here to set data['xx'] and data['yy'].
        See the code for their values.
        """
        print(nu, beta, beta_c) # Might log a lot, but it's slow
        if not 'Z' in list(self.data.values())[0].keys():
            self.compute_Z()
        L_min = np.min(list(self.data.keys()))
        for L, data in sorted(self.data.items(), key=lambda x: x[0]):
            data['xx'] = bb
            local_bb = np.power(L_min/L, 1/nu)*(bb-beta_c)/beta_c+beta_c
            span = [s for s in self.spans 
                    if int(s.simulations[0].kwargs['L']) == L][0]
            local_ZZ = np.array([obs_avg(b, span, data['Z'], 
                                         loaded=True, DISCARD=800) 
                                 for b in local_bb], dtype=np.float128)
            mh_mm = np.array([
                obs_avg(b, span, data['Z'], 
                             obs=lambda simul: np.abs(simul.data['m']), 
                             DISCARD=800, loaded=True)
                for b in local_bb
            ], dtype=np.float128) / local_ZZ
            data['yy'] = np.power(L/L_min, beta/nu)*mh_mm
            if plot:
                plt.plot(data['xx'], data['yy'], label=str(L))
        out_from_curves = np.array(
            [data['yy'] for data in self.data.values()], 
            dtype=np.float128
        )
        result = np.std(out_from_curves, axis=0)
        return np.array(result, dtype=np.float64)
    
def obs_avg(
    beta, # The beta at which to measure obs_avg
    span, # The span to use
    Zin, # The input Z_k values to use
    obs=lambda simul: 1.0, 
        # obs(simul) should return an array of obs values: simul might be loaded
    DISCARD=0,
    loaded=False,
    ):
    """
    Returns an array of Z_k, given input Zin_k, 
    with Z_k = \sum_E \frac{\sum_j N_j(E)}{\sum_j n_j Zin_j^-1 e^{-(b_j-b_k)E}}
    Of course getting the N_j would be a total suicide. 
    Let's just sum over the points:
    Z_k = \sum_p \frac{1}{\sum_j n_j Zin_j^-1 e^{-(b_j-b_k)E_p}}
    """
    # I suppose Zetas are ordered by betas
    result = 0
    bn = []

    simulations = []
    if not loaded:
        simulations = span.get_simulations()
    else:
        simulations = span.simulations

    for simul in simulations:
        betaj = float(simul.kwargs['b'])
        nj = len(simul.data['u'][DISCARD:])
        bn.append((betaj, nj))
    bn = sorted(bn, key=lambda x: x[0])
    assert len(bn)==len(Zin)
    for i, simul in enumerate(simulations): 
        # We have the points splitted among different datasets
        # We want to get parallel over p:
        denom = 0
        for j in range(len(Zin)):
            denom += (
                bn[j][1] / Zin[j] 
                * np.exp(
                    -(bn[j][0]-beta)*simul.data['u'][DISCARD:]
                    *int(simul.kwargs['model_size']),
                    dtype=np.float128
                )
            )
        try:
            obs_value = obs(simul)[DISCARD:]
        except:
            obs_value = obs(simul) # Sarà uno scalare
        result += np.sum(obs_value/denom)
    return result