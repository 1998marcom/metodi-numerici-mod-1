# Conti del modulo 1 dell'esame di Metodi Numerici

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  

- [Che cosa famo](#che-cosa-famo)
  - [Reticolo standard](#reticolo-standard)
  - [Cristallo cubico con corpo centrato](#cristallo-cubico-con-corpo-centrato)
  - [Cristallo cubico a facce centrate](#cristallo-cubico-a-facce-centrate)
  - [Diamante](#diamante)
- [Come lo famo](#come-lo-famo)
- [SubTasks](#subtasks)
  - [Simulare](#simulare)
  - [Analizzare](#analizzare)
- [Note che sono un abbozzo di documentazione](#note-che-sono-un-abbozzo-di-documentazione)
  - [Come compilare](#come-compilare)
  - [Formato furbo.txt](#formato-furbotxt)
    - [Reticolo semplice `lattice_cell.txt`](#reticolo-semplice-lattice_celltxt)
    - [Cristallo cubico con corpo centrato `bodyCenteredCubic_cell.txt`](#cristallo-cubico-con-corpo-centrato-bodycenteredcubic_celltxt)
    - [Cristallo cubico a facce centrate `faceCenteredCubic_cell.txt`](#cristallo-cubico-a-facce-centrate-facecenteredcubic_celltxt)
    - [Diamante (e qui son cazzi) `diamond_cell.txt`](#diamante-e-qui-son-cazzi-diamond_celltxt)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Che cosa famo
Conti su reticoli cristallini tridimensionali. CI aspettiamo stessi esponenti critici ma diverse temperature critiche. 

### Reticolo standard 
Va beh, è quello

### Cristallo cubico con corpo centrato
(Come si dice in Italiano?)

### Cristallo cubico a facce centrate
(Vedi sopra, non conosco l'italiano)

### Diamante
(Ho finito la voglia di definire un cristallo con nomi strani)

## Come lo famo
Possiamo andare in teoria sia di FSS che di RG. Ma il fatto che le cose non siano per niente invarianti di scala mi suggerisce che FSS sia moooolto meglio.

## SubTasks

### Simulare
- [x] Scrivere le funzioni che data una size, e una cella elementare in formato furbo.txt, sia in grado di sputare fuori il grafo per il resto del codice
- [ ] Scrivere i txt (oltre a quei 2 che avrò già usato come test)
- [ ] Decidere cosa salvare, compilare le simulazioni.
---
Da qui in poi bisogna fare su jupyterhub
- [ ] Scrivere gli script di wrapper (o il mega sh) per lanciare le simulazioni e lanciarle.

### Analizzare
- [ ] Scrivere gli script per tirare fuori le medie che ci interessano.
- [ ] Aggiustare gli script per tirare fuori le incertezze con Jackknife/bootstrap.
- [ ] Scrivere gli script che facciano i fit FSS per beta critical e gli esponenti critici, e tirino fuori le immagini.
Poi viene la relazione, ma è un altro paio di maniche

## Note che sono un abbozzo di documentazione

### Come compilare
```g++ crystal.cpp -o ../build/crystal -std=gnu++17 -ltbb -lfmt -fopenmp -fpermissive -w -O3```

Già, ma serve prima installare le librerie necessarie:
- HIP. In realtà non funziona veramente con ROCm e su CUDA, ma solo con
[HIP-CPU](https://github.com/ROCm-Developer-Tools/HIP-CPU): c'è la guida per installarlo linkata nel readme, per le diverse piattaforme.
- libfmt: `sudo apt install libfmt-dev`.
- TCLAP: `sudo apt install libtclap`.

### Formato furbo.txt
Per definire una cella fondamentale dobbiamo:
 - Dire quanti atomi ci sono (diciamo N)
 - Numerarli in qualche modo da 0 a N-1
 - Dire a quali è linkato ciascuno (e se alla cella lungo direzione x, y, z o se all'interno della stessa cella)
Definiamo i collegamenti alle altre celle in direzione lungo x, y, z come fatti verso la cella precedente,
ovvero con coordinata immediatamente inferiore a quella della cella in questione). Quelli verso la cella successiva
saranno gestiti della cella successiva, per cui questa cella risulterà la precedente.

Pertanto definiamo il formato dei file `<structure>_cell.txt`:
```
<N>
<a1> <a2> <a3>[; <x1> <x2>, <y1>, <z1>[; <xy1>, <xz1>, <yz1>[; <xyz1>]]]  # Questo è un commento
# ... altre N-1 righe come la precedente, una per ciascun elemento della cella
```
La prima riga indica il numero di atomi in una cella. Questi sono indicizzati da 0 a N.
Le successive N righe indicano, per ogni atomo della cella (dall'atomo 0 a quello N-1), quali sono i suoi link.
Per esempio `<a1> <a2> <a3>` sono dei numeri, distanziati da spazi bianchi, che identificano altri atomi
della stessa cella a cui è linkato l'atomo 0. Poi, `<x1> <x2>` indicano a quali atomi
della cella precedente (lungo l'asse x) è linkato l'atomo 0 di questa cella. Analogamente `<y1>` e `<z1>`.
Poi ancora `<xy1>` e affini indicano i link dell'atomo in questione con la cella che precede la cella in questione
di uno sull'asse x e di uno sull'asse y. Ovvero le due celle condividono uno spigolo.
Infine `<xyz1>` si spiega da solo.

Link ripetuti sono garantiti di essere contati una sola volta.

Nota per i babbani: le parentesi quadre indicano che un pezzo è opzionale.

In concreto abbiamo i file con le seguenti definizioni.
 
#### Reticolo semplice `lattice_cell.txt`
```text
1
;0,0,0
```

#### Cristallo cubico con corpo centrato `bodyCenteredCubic_cell.txt`
```text
2
1; 0 1, 0 1, 0 1; 1,1,1; 1
0
```

#### Cristallo cubico a facce centrate `faceCenteredCubic_cell.txt`
```text
4
1 2 3; 0 2 3, 0 1 3, 0 1 2; 3, 2, 1 
0
0
0
```

#### Diamante (e qui son cazzi) `diamond_cell.txt`
```text
# Lo faremo dopo
```
