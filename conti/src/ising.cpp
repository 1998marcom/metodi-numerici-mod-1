#ifndef FRAC_ISING_MH
#define FRAC_ISING_MH

#include <iostream>
#include <cstdlib>
#include <random>
#include <fstream>
#include <ctime>
#include <set>
#include <map>
using std::cout, std::endl;


#include <hip/hip_runtime.h>
#include "common.cpp"
#include "mt.cpp"

#ifndef GPU_COMPUTING
#include <cmath>
#endif

#define GRID_FOR(ix, N) for(unsigned int ix=threadIdx.x+blockIdx.x*blockDim.x; ix<N; ix+=gridDim.x*blockDim.x)

__global__
void metro_dense_kernel_attempt(ModelStruct gpu_model_str, const float p_flip) {
	GRID_FOR(i, gpu_model_str.size) {
		float randm = random_uniform_01();
		gpu_model_str.new_spins[i] = (randm < p_flip) ? (-gpu_model_str.spins[i]) : gpu_model_str.spins[i];
	}
}

__global__
void metro_dense_kernel_finalize(ModelStruct gpu_model_str, const float beta, const float J, const float B) {
	GRID_FOR(i, gpu_model_str.size) {
		if (gpu_model_str.spins[i] == gpu_model_str.new_spins[i]) continue;
		//std::cout << "Not continue" <<std::endl;
		int8_t difference_accumulator = 0; // Se le GPU vecchie non dovessero supportare int8_t e uint8_t un giorno lo cambierò
		FOR(j, TWO_DIMS) {
			if (j<gpu_model_str.links[i]) {
				//std::cout << "linked spin " << (int) gpu_model_str.spins[gpu_model_str.linked_spin_id[j][i]] <<std::endl;
				//std::cout << "linked new_spin " << (int) gpu_model_str.new_spins[gpu_model_str.linked_spin_id[j][i]] <<std::endl;
				difference_accumulator = 
					difference_accumulator ||
					( gpu_model_str.spins[gpu_model_str.linked_spin_id[j][i]] != gpu_model_str.new_spins[gpu_model_str.linked_spin_id[j][i]] ) ;
			}
				// da notare che sarebbe equivalente a FOR(j, gpu_model_str.links[i]), ma probabilmente così è più veloce
		}
		//std::cout << "difference_acc "<< (int) difference_accumulator <<std::endl;
		if (not difference_accumulator) {
			// Qua sta il Metropolis
			difference_accumulator = 0; // Now we use it as a proper counter
			int8_t my_spin = gpu_model_str.new_spins[i];
			FOR(j, TWO_DIMS) {
				difference_accumulator += (j<gpu_model_str.links[i]) ? (my_spin*gpu_model_str.spins[gpu_model_str.linked_spin_id[j][i]]) : 0 ;
			}
			float energy_diff = - 2*difference_accumulator*J - 2*my_spin*B; // HAMILTONIAN HERE (We are writing E2-E1)
			if (energy_diff < 0) {
				gpu_model_str.spins[i] = my_spin;
				//std::cout << "Flip" << std::endl;
			} else {
				float metro_p = exp(-beta*energy_diff);
				float randm = random_uniform_01();
				if (randm < metro_p) {
					gpu_model_str.spins[i] = my_spin;
					//std::cout << "Flip" << std::endl;
				} else {
					//std::cout << "Flip no" << std::endl;
				}
			}
		}
	}
}

/** TODO 
__global__
void metro_sparse_kernel_attempt(ModelStruct gpu_model_str, unsigned int* flip_table, const unsigned int n_flip) {
	FOR 

}

__global__
void metro_sparse_kernel_finalize(
	ModelStruct gpu_model_str, unsigned int* flip_table, const unsigned int n_flip,
	const float beta, const float J, const float B,
	) {

} */

// Then we would need the Wolff kernels (TODO)

class IsingModel {
	private:
		std::mt19937 cpu_random_generator;
		std::uniform_int_distribution<unsigned int> cpu_distribution_unsigned;
		std::uniform_real_distribution<float> cpu_distribution_float_01;
		ModelStruct gpu_model_str;
		bool on_gpu;
		unsigned int last_n_flip = 0;
		unsigned int* flip_table;
		__host__
		void to_gpu() {
			if (on_gpu) return;
			on_gpu = true;
			hipMemcpyHtoD(gpu_model_str.spins, cpu_model_str.spins, cpu_model_str.size*sizeof(int8_t));
		}
		__host__
		void to_cpu() {
			if (not on_gpu) return;
			on_gpu = false;
			hipMemcpyDtoH(cpu_model_str.spins, gpu_model_str.spins, cpu_model_str.size*sizeof(int8_t));
			magnetization = 0; // This is slow
			FOR(i, cpu_model_str.size) {
				magnetization += cpu_model_str.spins[i];
			}
		}
		long long int magnetization; // Slow if data is on GPU - but could be implemented faster
		long long int bonds_energy; // TODO
		bool quit_flag = false;
	public:
		float J = 1;
		float beta = 0.25;
		float B = 0;
		ModelStruct cpu_model_str; // it's public so that copying it's not needed, you can access its members and build it in-place
		std::map<unsigned long long, unsigned int> id_to_i;
		IsingModel(const unsigned int& size) : cpu_distribution_float_01(0, 1) {
			cpu_model_str.size = size;
			cpu_model_str.spins = malloc(size*sizeof(int8_t)); // Dio perdoni il malloc, ma dato che sotto devo usare hipMalloc ...
			cpu_model_str.new_spins = malloc(size*sizeof(int8_t));
			cpu_model_str.links = malloc(size*sizeof(uint8_t));
			memset(cpu_model_str.links, 0, size*sizeof(uint8_t));
			FOR(i, TWO_DIMS) {
				cpu_model_str.linked_spin_id[i] = malloc(size*sizeof(unsigned int));
			}
			gpu_model_str.size = size;
			hipMalloc(& gpu_model_str.spins, size*sizeof(int8_t));
			hipMalloc(& gpu_model_str.new_spins, size*sizeof(int8_t));
			hipMalloc(& gpu_model_str.links, size*sizeof(uint8_t));
			FOR(i, TWO_DIMS) {
				hipMalloc(& gpu_model_str.linked_spin_id[i], size*sizeof(unsigned int));
			}
			on_gpu = false;
			init_generators();
			cpu_random_generator.seed(time(NULL));
		}
		std::vector<double> get_quantities() { // Asking following qtys: energy_per_spin, magnetization_per_spin
			std::vector<double> result;
			double energy = ((double) get_energy()) / cpu_model_str.size;
			double magnet = ((double) get_magnetization()) / cpu_model_str.size;
			result.push_back(energy);
			result.push_back(magnet);
			return result;
		}
		void quit() {
			quit_flag = true;
		}
		~IsingModel() { 
			if (quit_flag) return; // Segfault stocastico in uscita di programma perché suppongo che la memoria si liberi in async;
			free(cpu_model_str.spins); free(cpu_model_str.new_spins); free(cpu_model_str.links);
			FOR(i, TWO_DIMS) {
				free(cpu_model_str.linked_spin_id[i]);
			}
			hipFree(gpu_model_str.spins); hipFree(gpu_model_str.new_spins); hipFree(gpu_model_str.links);
			FOR(i, TWO_DIMS) {
				hipFree(gpu_model_str.linked_spin_id[i]);
			}
		}
		int safe_link(unsigned int x, unsigned int y) {
			//cout << "Safe linking " << x << " to " << y << endl;
			unsigned int links_x = cpu_model_str.links[x];
			unsigned int links_y = cpu_model_str.links[y];
			if (links_x >= TWO_DIMS or links_y >= TWO_DIMS) {
				return -1;
			} else {
				auto lids = linked_ids(x);
				if (lids.count(y)) {
					return -2;
				} else {
					cpu_model_str.linked_spin_id[links_x][x] = y;
					cpu_model_str.links[x] += 1;
					cpu_model_str.linked_spin_id[links_y][y] = x;
					cpu_model_str.links[y] += 1;
					return 0;
				}
			}
		}
		bool test_link(unsigned int x, unsigned int y) {
			FOR(j, cpu_model_str.links[x]) {
				if (cpu_model_str.linked_spin_id[j][x] == y) return true;
			}
			return false;
		}
		std::set<unsigned int> linked_ids(unsigned int x, unsigned int distance = 1, bool exact=true) {
			std::set<unsigned int> result;
			result.insert(x);
			if (distance > 0) {
				FOR(i, cpu_model_str.links[x]) {
					std::set<unsigned int> tmp = linked_ids(cpu_model_str.linked_spin_id[i][x], distance-1);
					result.insert(tmp.begin(), tmp.end());
				}
			}
			if (exact and distance > 0) {
				std::set<unsigned int> diff = linked_ids(x, distance-1);
				for(auto& el : diff) {
					result.erase(el);
				}
			}
			return result;
		}
		std::set<unsigned int> get_neighbours(unsigned int x, unsigned int distance = 1, bool exact=true) {
			return linked_ids(x, distance, exact);
		}
		uint8_t count_links(unsigned int x) {
			return cpu_model_str.links[x];
		}
		int8_t* get_spins() {
			to_cpu();
			return cpu_model_str.spins;
		}
		void refresh_spins() {
			on_gpu = false;
			magnetization = 0;
			FOR(i, cpu_model_str.size) {
				magnetization += cpu_model_str.spins[i];
			}
		}
		long long int get_magnetization() const { to_cpu(); return magnetization; }
		double get_energy() const {
			to_cpu();
			long long int link_align = 0;
			FOR(i, cpu_model_str.size) {
				FOR(j, cpu_model_str.links[i]) {
					unsigned int link_id = cpu_model_str.linked_spin_id[j][i];
					link_align += cpu_model_str.spins[i] * cpu_model_str.spins[link_id];
				}
			}
			return - J/2.0 * link_align - B * magnetization;
		}
		void copy_graph_to_gpu() {
			gpu_model_str.size = cpu_model_str.size;
			hipMemcpyHtoD(gpu_model_str.links, cpu_model_str.links, cpu_model_str.size*sizeof(uint8_t));
			FOR(i, TWO_DIMS) {
				hipMemcpyHtoD(gpu_model_str.linked_spin_id[i], cpu_model_str.linked_spin_id[i], cpu_model_str.size*sizeof(unsigned int));
			}
		}

		__host__
		void step_metro_dense(const float& p_flip) { 
			// In realtà accedere a un grafico con i puntatori può essere inefficiente sulla GPU,
			// vedremo, alla peggio si può sempre fare sfruttando il multicore
			if (not on_gpu) to_gpu();
			//std::cout << " Metro dense kernel attempt " << std::endl;
			hipLaunchKernelGGL(
					metro_dense_kernel_attempt, dim3(N_BLOCKS), dim3(THREADS_PER_BLOCK),
					0, nullptr,
					gpu_model_str, p_flip);
			hipDeviceSynchronize();
			//std::cout << " Metro dense kernel finalize " << std::endl;
			hipLaunchKernelGGL(
					metro_dense_kernel_finalize, dim3(N_BLOCKS), dim3(THREADS_PER_BLOCK),
					0, nullptr,
					gpu_model_str, beta, J, B);
		}

		/** TODO
		__host__
		void step_metro_sparse(const unsigned int& n_flip) { // Secondo me questo sulla CPU va più veloce
			if (not on_gpu) to_gpu();
			if (n_flip != last_n_flip) {
				last_n_flip = n_flip;
				hipFree(flip_table);
				hipMalloc(&flip_table, n_flip*sizeof(unsigned int));
			}
			hipLaunchKernelGGL(
					metro_sparse_kernel_attempt, dim3(N_BLOCKS), dim3(THREADS_PER_BLOCK),
					0, nullptr,
					gpu_model_str, flip_table, n_flip); // ok sparse, but to have a slight efficiency please use n_flip >= N_BLOCKS*THREADS_PER_BLOCK
			hipDeviceSynchronize();
			hipLaunchKernelGGL(
					metro_sparse_kernel_finalize, dim3(N_BLOCKS), dim3(THREADS_PER_BLOCK),
					0, nullptr,
					gpu_model_str, flip_table, n_flip, beta, J , B);
		} */

		/** TODO
		__host__
		void step_metro_sparse_cpu(unsigned int n_flip) { // Pertanto lasciamo una implementazione per la CPU
			// qualcuno ha detto #pragma omp parallel for?
		} */

		/** TODO
		__host__
		void step_wolff_smart() {
		} */

		
		__host__
		void step_wolff_single_core() { // Meglio sempre provare anche le soluzioni tradizionali
			if (on_gpu) to_cpu();
			unsigned int start_idx = cpu_distribution_unsigned(cpu_random_generator) % cpu_model_str.size;
			float P_add = 1 - exp(-2*beta*J); // Supposing J > 0
			std::set<unsigned int> inside;
			std::set<unsigned int> frontier;
			inside.insert(start_idx);
			frontier.insert(start_idx);
			long long int magnetization_change = 0;
			long long int bonds_energy_change = 0;
			while(not frontier.empty()) {
				std::set<unsigned int> new_frontier;
				for(auto it=frontier.begin(); it != frontier.end(); it++) {
					magnetization_change += -2*cpu_model_str.spins[*it];
					FOR(j, cpu_model_str.links[*it]) {
						unsigned int linked_id = cpu_model_str.linked_spin_id[j][*it];
						if (not inside.count(linked_id) and cpu_model_str.spins[linked_id] == cpu_model_str.spins[*it]) {
							float randm = cpu_distribution_float_01(cpu_random_generator);
							if (randm < P_add)
								new_frontier.insert(linked_id);
						}
					}
				}
				frontier = new_frontier;
				inside.insert(new_frontier.begin(), new_frontier.end());
			}
			float energy_diff = 2*B*inside.size()*cpu_model_str.spins[start_idx]; // HAMILTONIAN HERE (We are writing part of E2-E1)
			float randm = cpu_distribution_float_01(cpu_random_generator);
			if (energy_diff < 0 or randm < exp(-beta*energy_diff)) {
				for (auto it=inside.begin(); it != inside.end(); it++) {
					cpu_model_str.spins[*it] = -cpu_model_str.spins[*it];
				}
				magnetization += magnetization_change;
			}
		}

		friend std::ostream& operator<< (std::ostream& stream, IsingModel& ising);

};

std::ostream& operator<< (std::ostream& stream, IsingModel& ising) {
	std::vector<double> quantities = ising.get_quantities();
	for(const auto& qty: quantities) {
		stream << qty << "\t";
	}
	stream << "\n";
	return stream;
}

#endif
