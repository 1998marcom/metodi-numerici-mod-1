#include "ising.cpp"
#include <random>
#include <cmath>
#include <iostream>
#include <map>
#include <ctime>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <tclap/CmdLine.h>
#include <fmt/format.h>
#include <vector>

typedef unsigned int uint;
using std::cout, std::endl;
using namespace std;

template <typename T> int sgn(T val) {
	    return (T(0) < val) - (val < T(0));
}

typedef struct {
	std::vector<uint> same;
	std::vector<uint> x;
	std::vector<uint> y;
	std::vector<uint> z;
	std::vector<uint> xy;
	std::vector<uint> xz;
	std::vector<uint> yz;
	std::vector<uint> xyz;
} AtomLinks;

typedef struct{
	uint N;
	std::vector<AtomLinks> links;
	// See the README.md to get the meaning of the vector names
} CrystalCell;

vector<uint> load_space_sep_string(const string input) {
	stringstream sline(input);
	vector<uint> result;
	uint temp;
	while(true) {
		sline >> temp;
		if (!sline)
			break;
		else
			result.push_back(temp);
	}
	return result;
}
CrystalCell parse_cellfile(const std::string & path) {
	fstream file(path);
	CrystalCell cell;
	
	{
		string line;
		stringstream sline;
	
		getline(file, line);
		sline.str(line);
		sline >> cell.N;
	}

	cell.links.resize(cell.N);

	try {
		for (uint i=0; i<cell.N; i++) {
			string line;
			getline(file, line);
			uint linetype = count(line.begin(), line.end(), ';');
			uint last_semicolon, comma_2, comma_1;
			string buffer;
			switch (linetype) {
				case 3:
					last_semicolon = line.find_last_of(';');
					cell.links[i].xyz = load_space_sep_string(line.substr(last_semicolon+1));
					line = line.substr(0, last_semicolon);
				case 2:
					last_semicolon = line.find_last_of(';');
					comma_2 = line.find_last_of(',');
					comma_1 = line.find_last_of(',', comma_2-1);

					cell.links[i].xy = load_space_sep_string(line.substr(last_semicolon+1,comma_1-last_semicolon-1));
					cell.links[i].xz = load_space_sep_string(line.substr(comma_1+1,comma_2-comma_1-1));
					cell.links[i].yz = load_space_sep_string(line.substr(comma_2+1));

					line = line.substr(0, last_semicolon);
				case 1:
					last_semicolon = line.find_last_of(';');
					comma_2 = line.find_last_of(',');
					comma_1 = line.find_last_of(',', comma_2-1);

					cell.links[i].x = load_space_sep_string(line.substr(last_semicolon+1,comma_1-last_semicolon-1));
					cell.links[i].y = load_space_sep_string(line.substr(comma_1+1,comma_2-comma_1-1));
					cell.links[i].z = load_space_sep_string(line.substr(comma_2+1));

					line = line.substr(0, last_semicolon);
				case 0:
					cell.links[i].same = load_space_sep_string(line);
			}
		}
	} catch (const std::exception& e) {
		cout << "The cell file seems wrongly formatted. I'll try to continue.\n"
			 << "Below you can find further information about the exception.\n";
		cout << e.what(); //Porcoddue spero che sia questo
	}
	return cell;
}

typedef array<uint, 4> Coord; // So that we have x, y, z, n where n is the index of the atom of the x,y,z cell

uint coord_to_index(const Coord & crd, const uint & L, const CrystalCell & cell) {
	return ( (crd[0]*L+crd[1])*L + crd[2] ) * cell.N + crd[3];
}

Coord index_to_coord(uint index, const uint & L, const CrystalCell & cell) {
	Coord crd;
	crd[3] = index % cell.N;
	index = index / cell.N;
	crd[2] = index % L;
	index = index / L;
	crd[1] = index % L;
	index = index / L;
	crd[0] = index;
	return crd;
}

void soft_link(IsingModel& model, const uint & L, const CrystalCell & cell, 
		const Coord crd, const Coord cell_crd, const vector<uint> & cell_links) {
	if ( cell_crd[0] == (unsigned int) -1 or cell_crd[1] == (unsigned int) -1 or cell_crd[2] == (unsigned int) -1 ) return;
	uint index = coord_to_index(crd, L, cell);
	uint cell_index = coord_to_index(cell_crd, L, cell); // Porcoddio fa sì che arrivi già cell_crd[3] == 0
	for(auto lit: cell_links) {
		model.safe_link(index, cell_index + lit);
	}
}
void bovine_link(IsingModel& model, const uint & index, const uint & L, const CrystalCell & cell) {
	Coord crd = index_to_coord(index, L, cell);
	AtomLinks links = cell.links[crd[3]];
	Coord base_crd = crd; base_crd[3] = 0;
	
	Coord cell_crd = base_crd;
	soft_link(model, L, cell, crd, cell_crd, links.same);

	cell_crd = base_crd; cell_crd[0]--;
	soft_link(model, L, cell, crd, cell_crd, links.x);
	cell_crd = base_crd; cell_crd[1]--;
	soft_link(model, L, cell, crd, cell_crd, links.y);
	cell_crd = base_crd; cell_crd[2]--;
	soft_link(model, L, cell, crd, cell_crd, links.z);

	cell_crd = base_crd; cell_crd[0]--; cell_crd[1]--;
	soft_link(model, L, cell, crd, cell_crd, links.xy);
	cell_crd = base_crd; cell_crd[0]--; cell_crd[2]--;
	soft_link(model, L, cell, crd, cell_crd, links.xz);
	cell_crd = base_crd; cell_crd[1]--; cell_crd[2]--;
	soft_link(model, L, cell, crd, cell_crd, links.yz);

	cell_crd = base_crd; cell_crd[0]--; cell_crd[1]--; cell_crd[2]--;
	soft_link(model, L, cell, crd, cell_crd, links.xyz);
}

void construct(IsingModel& model, const uint L, const CrystalCell & cell) {
	// Questa funzione è da implementare per generare il grafo su cui lavorare
	unsigned int size = model.cpu_model_str.size;
	std::mt19937 my_generator(time(NULL));

	for (uint i = 0; i < size; i++) {
		bovine_link(model, i, L, cell);
	}
	cout << "Construction ended with size " << size;

}
		
int main(int argc, char* argv[]) {
	
	// argv = [ env_dim, n, extra_occupation, model_scale, sim_time, beta, outfile_name ]
	// model_size = vedi giù
	try {
		TCLAP::CmdLine parser("Programma per simulare ising su reticoli cristallini 3D.");
		TCLAP::ValueArg<uint> ARG_L(
				/* flag */ "L",
				/* name */ "size",
				/* desc */ "Linear size of the model, in terms of number of fundamental cells.",
				/* req  */ true,
				/* defau*/ 10,
				/* typed*/ "An unsigned integer");
		parser.add(ARG_L);
		TCLAP::ValueArg<std::string> ARG_cellfile(
				/* flag */ "f",
				/* name */ "cellfile",
				/* desc */ "Where to pick the file that defines the cell. See the docs for information about the format used.",
				/* req  */ true,
				/* defau*/ "NULL",
				/* typed*/ "A relative path");
		parser.add(ARG_cellfile);
		TCLAP::ValueArg<std::string> ARG_sim_time(
				/* flag */ "t",
				/* name */ "time",
				/* desc */ "Steps of simulation to perform: each step involves a dense Metropolis-Hastings (with settable flipping probability). \
Each 10 steps the simulation variables are recorded. Each 100 steps a Wolff step is performed and variables are printed on stdout.",
				/* req  */ true,
				/* defau*/ "10",
				/* typed*/ "An unsigned integer");
		parser.add(ARG_sim_time);
		TCLAP::ValueArg<double> ARG_pflip(
				/* flag */ "p",
				/* name */ "pflip",
				/* desc */ "Base probability of flipping for the dense metropolis-hastings",
				/* req  */ false,
				/* defau*/ 0.07,
				/* typed*/ "A floating point value");
		parser.add(ARG_pflip);
		TCLAP::ValueArg<double> ARG_beta(
				/* flag */ "b",
				/* name */ "beta",
				/* desc */ "Ising model beta parameter",
				/* req  */ true,
				/* defau*/ 0.1,
				/* typed*/ "A floating point value");
		parser.add(ARG_beta);
		TCLAP::ValueArg<double> ARG_J(
				/* flag */ "J",
				/* name */ "coupling",
				/* desc */ "Coupling parameter between the spins of the Ising model",
				/* req  */ false,
				/* defau*/ 1,
				/* typed*/ "A floating point value");
		parser.add(ARG_J);
		TCLAP::ValueArg<double> ARG_B(
				/* flag */ "B",
				/* name */ "field",
				/* desc */ "External field of the Ising model",
				/* req  */ false,
				/* defau*/ 0,
				/* typed*/ "A floating point value");
		parser.add(ARG_B);
		TCLAP::ValueArg<std::string> ARG_outfile(
				/* flag */ "o",
				/* name */ "outfile",
				/* desc */ "Where to output the simulation results",
				/* req  */ false,
				/* defau*/ "NULL",
				/* typed*/ "A relative path");
		parser.add(ARG_outfile);

		parser.parse(argc, argv);

		CrystalCell cell = parse_cellfile(ARG_cellfile.getValue());
		uint L = ARG_L.getValue();

		unsigned long long int sim_time = stoll(ARG_sim_time.getValue());
		double beta = ARG_beta.getValue();
		double J = ARG_J.getValue();
		double B = ARG_B.getValue();
		double pflip = ARG_pflip.getValue();

		// Building the model
		unsigned int model_size = L * L * L * cell.N;
		IsingModel model(model_size);
		cout << "Beginning construction" << endl;
		
		construct(model, L, cell);
		cout << "Ended construction" << endl << endl;
		model.copy_graph_to_gpu();

		// Initializing a non-thermalized state
		FOR(i, model_size) {
			model.cpu_model_str.spins[i] = 1;
		}
		model.refresh_spins();

		model.beta = beta;
		model.J = J;
		model.B = B;
		
		std::string outfile_name = ARG_outfile.getValue();
		std::fstream outfile_header;
		outfile_header.open(outfile_name+"_header", std::fstream::out | std::fstream::trunc);
		outfile_header << "L\tcell.N\tf\tt\tb\tJ\tB\tp\tmodel_size" << endl;
		outfile_header << L << " ";
		outfile_header << cell.N << " ";
		outfile_header << ARG_cellfile.getValue() << " ";
		outfile_header << sim_time << " ";
		outfile_header << beta << " ";
		outfile_header << J << " ";
		outfile_header << B << " ";
		outfile_header << pflip << " ";
		outfile_header << model_size << " ";
		outfile_header.close();
		std::fstream outfile;
		if (outfile_name != "NULL") {
			outfile.open(outfile_name, std::fstream::in | std::fstream::out | std::fstream::trunc);
			outfile << "#Energy_per_spin\tMagnetization_per_spin" << "\n";
		}

		// Simulating
		FOR(t, sim_time) {
			if (t%10 == 0) {
				if (outfile_name != "NULL") {
					outfile << model;
				}
			}
			if(t%100) {
				model.step_metro_dense(pflip);
			} else {
				cout << "@t=" << t << "\n";
				auto qtys = model.get_quantities();
				cout << fmt::format("\tModel\tenergy_per_spin: {:03.3f}\tmagnetization: {:03.3f}\n", qtys[0], qtys[1]);
				model.step_wolff_single_core();
			}
		}
		outfile.close();
		
		model.quit();
	} catch (TCLAP::ArgException &e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}
	return 0;
}
